/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.animal;

/**
 *
 * @author Ow
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human h1 = new Human("George");
        h1.eat();
        h1.walk();
        h1.run();
        
        Cat c1 = new Cat("Yui");
        c1.eat();
        c1.walk();
        c1.run();
        
        Dog d1 = new Dog("Eddy");
        d1.eat();
        d1.walk();
        d1.run();
        
        Snake s1 = new Snake("Alan");
        s1.eat();
        s1.walk();
        s1.crawl();
        
        Crocodile cd1 = new Crocodile("Payu");
        cd1.eat();
        cd1.walk();
        cd1.crawl();
        
        Fish f1 = new Fish("Toey");
        f1.eat();
        f1.sleep();
        f1.swim();
        
        Crab cr1 = new Crab("Momo");
        cr1.eat();
        cr1.sleep();
        cr1.swim();
        
        Bat b1 = new Bat("Sam");
        b1.eat();
        b1.sleep();
        b1.fly();
        
        Bird bd1 = new Bird("Zone");
        bd1.eat();
        bd1.sleep();
        bd1.fly();

        
    }
    
}
